﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World {
    Tile[,] tiles;

    public int width { get; private set; }
    public int height { get; private set; }

    public World(int width, int height)
    {
        this.width = width;
        this.height = height;

        tiles = new Tile[width, height];

        for(int x = 0; x < width; x++)
        {
            for(int y = 0; y < height; y++)
            {
                tiles[x, y] = new Tile(this, x, y);
            }
        }
    }

    public Tile getTile(int x, int y)
    {
        return tiles[x, y];
    }

    
}
