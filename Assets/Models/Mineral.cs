﻿using UnityEngine;
using System.Collections;

public class Mineral
{
    public string name { get; private set; }
    public int value { get; private set; }
    public float mass { get; private set; }
    private AnimationCurve chanceCurve;

    public float chance(int depth)
    {
        return chanceCurve.Evaluate((float)depth);
    }

    private Mineral(string name, int value, Keyframe[] chanceKeyframes, float mass)
    {
        chanceCurve = new AnimationCurve();

        foreach(var frame in chanceKeyframes)
        {
            chanceCurve.AddKey(frame);
        }

        this.name = name;
        this.value = value;
        this.mass = mass;
    }

    public static Mineral iron = new Mineral("iron", 5, new Keyframe[] {
        new Keyframe(0, 0.1f),
        new Keyframe(8, 0.1f),
        new Keyframe(12, 0.0f)
    }, 0.05f);

}
