﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile {
    public int x { get; private set; }
    public int y { get; private set; }
    public World world { get; private set; }

    public bool dug { get; private set; }
    public Mineral mineral { get; private set; }

    private bool _mark;




    public delegate void redrawDelegate(Tile tile);

    public redrawDelegate redraw;

    public Tile(World world, int x, int y)
    {
        this.x = x;
        this.y = y;
        this.world = world;


    }

    public void update(bool dug, Mineral mineral)
    {
        this.dug = dug;
        this.mineral = mineral;

        if (redraw != null)
            redraw(this);
    }




    public bool isDiggable()
    {
        return !isSupport();
    }

    public bool isDestructible()
    {
        return !isSupport();
    }

    public bool mark
    {
        get { return _mark; }
        set { _mark = value; if(redraw != null) redraw(this); }
    }

    public bool isSupport()
    {
        return y == 0 && x >= 3 && x <= 6;
    }
}
