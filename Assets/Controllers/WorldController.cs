﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldController : MonoBehaviour {

    public int width;
    public int height;
    public World world { get; private set; }

    public Sprite grass;
    public Sprite ground;
    public Sprite mineral;

    // Use this for initialization
    void Start () {
        world = new World(width, height);

        // Init world
        for(int x = 0; x < width; x++)
        {
            for(int y = 0; y<height; y++)
            {
                Tile t = world.getTile(x, y);

                
                    GameObject go = new GameObject();
                    go.name = string.Format("tile_{0}_{1}", x, y);

                    go.transform.position = new Vector3(0.5f + x, -0.5f + (y * -1f), 0);

                    go.transform.SetParent(transform, true);

                    var background = go.AddComponent<SpriteRenderer>();

                  
                

                GameObject go2 = new GameObject();
                go2.name = string.Format("tile_{0}_{1}_fg", x, y);

                go2.transform.position = new Vector3(0.5f + x, -0.5f + (y * -1f), 0);

                go2.transform.SetParent(transform, true);
                var foreground = go2.AddComponent<SpriteRenderer>();
                foreground.sortingOrder = 1;

                t.redraw += (tile) => redrawTile(tile, go, background, foreground);

                Mineral m = null;

                if (!t.isSupport())
                {
                    if (m == null && Random.value < Mineral.iron.chance(y))
                        m = Mineral.iron;
                }

                t.update(false, m);
            }
        }

    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Escape))
            quit();
    }

    public void redrawTile(Tile tile, GameObject go, SpriteRenderer background, SpriteRenderer foreground)
    {
        if (tile.dug)
        {
            background.enabled = false;
            foreground.enabled = false;
        }
        else
        {
            if (tile.y == 0) background.sprite = grass;
            else background.sprite = ground;

            if (tile.mineral == null)
                foreground.enabled = false;
            else
            {
                foreground.sprite = mineral;
                foreground.enabled = true;
            }
        }

        if (tile.mark)
            background.color = Color.red;
        else
            background.color = Color.white;
    }

    public void quit()
    {
        Application.Quit();
    }

    
}
