﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyPopupController : MonoBehaviour {

    AnimationCurve fade;
    AnimationCurve rise;
    float timePassed = 0;
    float sourceY = 0;

	// Use this for initialization
	void Start () {
        fade = new AnimationCurve(new Keyframe[]
        {
            new Keyframe(0f, 1f),
            new Keyframe(2f, 1f),
            new Keyframe(3f, 0f)
        });

        rise = new AnimationCurve(new Keyframe[]
        {
            new Keyframe(0f, 0f),
            new Keyframe(3f, 0.5f)
        });

        timePassed = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (timePassed == 0) sourceY = transform.position.y;

        float opacity = fade.Evaluate(timePassed);

        if (opacity == 0f)
            Destroy(this);
        else
        {
            var mesh = GetComponent<TextMesh>();
            mesh.color = new Color(255, 255, 255, opacity);
        }

        transform.position = new Vector3(transform.position.x, sourceY + rise.Evaluate(timePassed), transform.position.z);
        
        timePassed += Time.deltaTime;
	}

    public void setSum(int sum)
    {
        var mesh = GetComponent<TextMesh>();
        mesh.text = string.Format("{0}$", sum);
    }
}
