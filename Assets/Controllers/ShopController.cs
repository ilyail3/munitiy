﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopController : MonoBehaviour {
    public void OnTriggerEnter2D(Collider2D collision)
    {
        var digger = collision.GetComponent<DiggerController>();
        if (digger != null)
            digger.sellMinerals();
    }

}
