﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiggerController : MonoBehaviour {

    float g = 10f;
    float mass = 1f;
    Vector2 velocity;

    float vThrust = 12;
    float hThrust = 5;

    private float offsetX = 0.03f;
    private float offsetBottom = 0.12f;

    AnimationCurve thrustCurve;

    public WorldController worldController;
    public Text moneyText;
    public MoneyPopupController moneyPopup;

    private List<Mineral> minerals;
    private float boostTime = 0f;

    public int money { get; private set; }

	// Use this for initialization
	void Start () {
        thrustCurve = new AnimationCurve();
        thrustCurve.AddKey(worldController.height * -1f, 0.3f);
        thrustCurve.AddKey(-10f, 0.7f);
        thrustCurve.AddKey(0, 1f);
        thrustCurve.AddKey(1.5f, 0.9f);
        thrustCurve.AddKey(2.5f, 0);

        minerals = new List<Mineral>();

    }
	
	// Update is called once per frame
	void Update () {
        Vector2 gravity = new Vector2(0, mass * g * -1);
        Vector2 totalForces = gravity;

        if (Input.GetKey(KeyCode.UpArrow))
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                boostTime = 0.05f;
            } else if(boostTime > 0)
            {
                boostTime -= Time.deltaTime;
            }

            float bonus = boostTime > 0f ? 3f : 1f;
            totalForces += new Vector2(0, vThrust * thrustEfficiency() * bonus);
        }

        Vector2 vNormal = new Vector2(0, 0);

        // Do vertical normal force calculation
        if (Mathf.Abs(transform.position.y - getCollisionBottom()) < 0.01 && totalForces.y < 0)
        {
            vNormal = new Vector2(0, totalForces.y * -1);
            totalForces += vNormal;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            if(vNormal.magnitude > 0)
                // This vehicle develops twice the thrust if touching the ground
                // and that thrust is not dependent on depth(air density)
                totalForces += new Vector2(hThrust * 2f * -1f, 0);
            else
                totalForces += new Vector2(hThrust * thrustEfficiency() * -1, 0);
        } else if (Input.GetKey(KeyCode.RightArrow))
        {
            if (vNormal.magnitude > 0)
                // This vehicle develops twice the thrust if touching the ground
                // and that thrust is not dependent on depth(air density)
                totalForces += new Vector2(hThrust * 2f, 0);
            else
                totalForces += new Vector2(hThrust * thrustEfficiency(), 0);
        }

        

        // Calculate surface friction
        if(vNormal.magnitude > 0 && (velocity.x != 0 || totalForces.x != 0))
        {
            

            // Make sure I can actually stop from surface friction
            if (totalForces.x == 0 && Mathf.Abs(velocity.x) < vNormal.magnitude * frictionCoefficient() * Time.deltaTime)
            {
                velocity.x = 0;
            }
            else
            {
                float direction = 0;

                if (velocity.x != 0)
                    direction = Mathf.Sign(velocity.x);
                else
                    direction = Mathf.Sign(totalForces.x);

                Vector2 dragForce = new Vector2(direction * -1f * vNormal.magnitude * frictionCoefficient(), 0);

                totalForces += dragForce;
                    
            }
        }

        Vector2 hNormal = new Vector2(0, 0);
        if(Mathf.Abs(transform.position.x - getCollisionLeft()) < 0.01 && totalForces.x < 0)
        {
            hNormal = new Vector2(totalForces.x * -1, 0);
            totalForces += hNormal;
        }
        else if(Mathf.Abs(transform.position.x - getCollisionRight()) < 0.01f && totalForces.x > 0) 
        {
            hNormal = new Vector2(totalForces.x * -1, 0);
            totalForces += hNormal;
        }

        velocity = velocity + (totalForces / mass) * Time.deltaTime;

        transform.Translate(velocity * Time.deltaTime);

        float collisionBottom = getCollisionBottom();
        if (transform.position.y < collisionBottom && velocity.y < 0)
        {
            Debug.Log(string.Format("Lost {0:0.##} vertical velocity", Mathf.Abs(velocity.y)));
            velocity.y = 0;
            //transform.Translate(0, transform.position.y - collisionBottom, 0);
            transform.position = new Vector3(transform.position.x, collisionBottom, 0);
            
        }

        if (transform.position.y > getCollisionTop() && velocity.y > 0)
        {
            Debug.Log(string.Format("Lost {0:0.##} vertical velocity", Mathf.Abs(velocity.y)));
            velocity.y = 0;
            //transform.Translate(0, transform.position.y - collisionBottom, 0);
            transform.position = new Vector3(transform.position.x, getCollisionTop(), 0);

        }

        if (transform.position.x < getCollisionLeft() && velocity.x < 0)
        {
            Debug.Log(string.Format("Lost {0:0.##} horizontal velocity", Mathf.Abs(velocity.x)));
            velocity.x = 0;
            //transform.Translate((transform.position.x - getCollisionLeft()) * -1f, 0, 0);
            transform.position = new Vector3(getCollisionLeft(), transform.position.y, 0);
        }

        if (transform.position.x > getCollisionRight() && velocity.x > 0)
        {
            Debug.Log(string.Format("Lost {0:0.##} horizontal velocity", Mathf.Abs(velocity.x)));
            velocity.x = 0;
            transform.position = new Vector3(getCollisionRight(), transform.position.y, 0);
        }

        var vertExtent = Camera.main.orthographicSize;
        var horzExtent = vertExtent * Screen.width / Screen.height;

        // Move camera to follow character
        Vector3 cameraPos = transform.position;
        cameraPos.z = -10f;
        cameraPos.x = Mathf.Max(horzExtent, cameraPos.x);
        cameraPos.x = Mathf.Min(worldController.width - horzExtent, cameraPos.x);

        cameraPos.y = Mathf.Min(3f - vertExtent, cameraPos.y);
        Camera.main.transform.position = cameraPos;

        // Check if digging
        if(Input.GetKeyDown(KeyCode.DownArrow) && velocity.y == 0 && xTileStart() == xTileEnd())
        {
            Tile t = worldController.world.getTile(xTileStart(), depthBelow());

            if (!t.dug && t.isDiggable())
            {
                if (t.mineral != null)
                {
                    minerals.Add(t.mineral);
                    mass += t.mineral.mass;
                }

                t.update(true, null);
            }
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow) && velocity.x == 0 && yTileStart() == yTileEnd() && tileOnLeft() >= 0)
        {
            Tile t = worldController.world.getTile(tileOnLeft(), yTileStart());

            if (!t.dug && t.isDiggable())
            {
                if (t.mineral != null)
                    minerals.Add(t.mineral);

                t.update(true, null);
            }
        }

        if (Input.GetKeyDown(KeyCode.RightArrow) && velocity.x == 0 && yTileStart() == yTileEnd() && tileOnRight() < worldController.width)
        {
            Tile t = worldController.world.getTile(tileOnRight(), yTileStart());

            if (!t.dug && t.isDiggable())
            {
                if (t.mineral != null)
                    minerals.Add(t.mineral);

                t.update(true, null);
            }
        }
    }

    float thrustEfficiency()
    {

        return thrustCurve.Evaluate(transform.position.y);
    }

    float frictionCoefficient()
    {
        return 0.6f;
    }

    float getCollisionBottom()
    {
        if(transform.position.y < 0.5 + offsetBottom)
        {
            int depth = depthBelow();
            float retVal = float.NegativeInfinity;

            for (int x = xTileStart(); x <= xTileEnd(); x++)
            {
                Tile t = worldController.world.getTile(x, depth);

                //if (!t.dug) return depth + 0.5f - offsetBottom;
                if (!t.dug) retVal = (0.5f - offsetBottom) - depth;
            }

            return retVal;
        }

        return float.NegativeInfinity;
        //return 0.5f - offsetBottom;
    }

    float getCollisionTop()
    {
        float retVal = float.PositiveInfinity;

        if (transform.position.y < -0.63)
        {
            int depth = depthAbove();
            

            for (int x = xTileStart(); x <= xTileEnd(); x++)
            {
                Tile t = worldController.world.getTile(x, depth);

                //if (!t.dug) return depth + 0.5f - offsetBottom;
                if (!t.dug) retVal = (depth * -1) - 1.37f;
            }

            
        }

        return retVal;
        //return 0.5f - offsetBottom;
    }

    float getCollisionLeft()
    {
        int colideLeft = tileOnLeft();
        // Don't colide with anything on when stending above ground
        if (transform.position.y < 0.37 && colideLeft >= 0)
        {
            float retVal = 0.5f - offsetX;

            for(int y = yTileStart(); y >= yTileEnd(); y--)
            {
                Tile t = worldController.world.getTile(colideLeft, y);
                if (!t.dug) retVal = (colideLeft + 1) + (0.5f - offsetX);
            }

            return retVal;
        }
        return 0.5f - offsetX;
    }

    float getCollisionRight()
    {

        float retVal = worldController.world.width - 0.5f + offsetX;

        int colideRight = tileOnRight();
        // Don't colide with anything on when stending above ground
        if (transform.position.y < 0.37 && colideRight < worldController.width)
        {
            

            for (int y = yTileStart(); y >= yTileEnd(); y--)
            {
                Tile t = worldController.world.getTile(colideRight, y);
                if (!t.dug) retVal = colideRight - 0.5f + offsetX;
            }

            
        }

        return retVal;
    }

    private int xTileStart()
    {
        return (int)Mathf.Max(0, Mathf.Floor(transform.position.x - offsetX - 0.4f));
    }

    private int xTileEnd()
    {
        return (int)Mathf.Min(worldController.width - 1, Mathf.Floor(transform.position.x + offsetX + 0.4f));
    }

    private int yTileStart()
    {
        return (int)Mathf.Max(0, Mathf.Floor((transform.position.y - 0.35f) * -1));
    }

    private int yTileEnd()
    {
        return (int)Mathf.Max(0, Mathf.Floor((transform.position.y + 0.15f) * -1));
    }

    private int depthBelow()
    {
        return (int)Mathf.Max(0, Mathf.Floor((transform.position.y - 0.5f) * -1)); 
    }

    private int depthAbove()
    {
        return (int)Mathf.Max(0, Mathf.Floor((transform.position.y + 0.7f) * -1));
    }

    private int tileOnLeft()
    {
        return (int)Mathf.Floor(transform.position.x - offsetX - 0.9f);
    }

    private int tileOnRight()
    {
        return (int)Mathf.Floor(transform.position.x - offsetX + 1.2f);
    }

    public void sellMinerals()
    {
        if(minerals.Count > 0)
        {
            int profit = 0;

            while (minerals.Count > 0)
            {
                Mineral m = minerals[0];
                profit += m.value;
                mass -= m.mass;

                minerals.RemoveAt(0);
            }

            changeMoney(profit);
        }
        
    }

    private void changeMoney(int sum)
    {
        var popup = Instantiate(moneyPopup);
        popup.setSum(sum);
        popup.transform.position = transform.position + new Vector3(0, 0.5f, 0);

        //TODO display money popup
        money += sum;
        moneyText.text = string.Format("{0}$", money);
    }
}
